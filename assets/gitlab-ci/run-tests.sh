#!/bin/bash

##
# Run all tests!
RESULT=0

# Coding standards checks.
echo "Checking coding standards"
./bin/phpcs
if [ $? -ne 0 ]; then
  ((RESULT++))
fi

# Deprecated code checks.
echo "Checking for deprecated code"
./bin/phpstan analyse -c ./phpstan.neon ./web/profiles/contrib/resourcehub-distribution/
if [ $? -ne 0 ]; then
  ((RESULT++))
fi

# PHPUnit tests.
echo "Running tests"
su docker -c "./bin/phpunit --testdox"
if [ $? -ne 0 ]; then
  ((RESULT++))
fi

echo $RESULT
# Set return code depending on number of tests that failed.
exit $RESULT
