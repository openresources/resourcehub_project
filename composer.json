{
    "name": "openresources/resourcehub-project",
    "description": "Project template for the Open Resources Resource Hub distribution.",
    "type": "project",
    "license": "GPL-2.0-or-later",
    "authors": [
        {
            "name": "Finn Lewis",
            "email": "finn@agile.coop"
        },
        {
            "name": "James Hall",
            "email": "james@agile.coop"
        },
        {
            "name": "Maria Young",
            "email": "maria@agile.coop"
        }
    ],
    "homepage": "https://gitlab.com/openresource/resourcehub_project",
    "repositories": [
        {
            "type": "composer",
            "url": "https://packages.drupal.org/8"
        },
        {
            "type": "composer",
            "url": "https://asset-packagist.org"
        }
    ],
    "require": {
        "composer/installers": "^1.10",
        "cweagans/composer-patches": "^1.6",
        "drupal/core-composer-scaffold": "^10.1@stable",
        "drupal/core-project-message": "^10.1@stable",
        "drupal/core-recommended": "^10.1@stable",
        "oomphinc/composer-installers-extender": "^2.0",
        "openresources/resourcehub-distribution": "^1.0@beta"
    },
    "require-dev": {
        "drush/drush": "~12",
        "brianium/paratest": "^6.0",
        "drupal/coder": "^8.3",
        "drupal/core-dev": "^10.1@stable",
        "mglaman/phpstan-drupal": "^1.0",
        "phpspec/prophecy-phpunit": "^2.0",
        "phpstan/phpstan-deprecation-rules": "^1.0",
        "squizlabs/php_codesniffer": "^3.6"
    },
    "conflict": {
        "drupal/drupal": "*"
    },
    "minimum-stability": "dev",
    "prefer-stable": true,
    "config": {
        "sort-packages": true,
        "bin-dir": "bin/",
        "allow-plugins": {
            "composer/installers": true,
            "cweagans/composer-patches": true,
            "drupal/core-composer-scaffold": true,
            "drupal/core-project-message": true,
            "oomphinc/composer-installers-extender": true,
            "dealerdirect/phpcodesniffer-composer-installer": true,
            "phpstan/extension-installer": true
        }
    },
    "extra": {
        "enable-patching": true,
        "patches": {
            "drupal/core": {
                "#2985168-166: Allow media items to be edited in a modal when using the field widget": "https://www.drupal.org/files/issues/2023-11-01/2985168-media-edit-modal-10.1.x-166.patch",
                "#2246725-34: Test runs can throw exceptions due to missing .htkey file": "https://www.drupal.org/files/issues/2018-11-30/2246725-34.patch"
            }
        },
        "drupal-scaffold": {
            "locations": {
                "web-root": "web/"
            },
            "file-mapping": {
                "[web-root]/sites/default/settings.php": {
                    "mode": "replace",
                    "path": "assets/composer/settings.php",
                    "overwrite": false
                },
                "[web-root]/sites/default/settings.lando.php": {
                    "mode": "replace",
                    "path": "assets/composer/settings.lando.php",
                    "overwrite": false
                },
                "[web-root]/sites/development.services.yml": "assets/composer/development.services.yml"
            }
        },
        "installer-types": ["bower-asset", "npm-asset"],
        "installer-paths": {
            "web/core": ["type:drupal-core"],
            "web/libraries/{$name}": [
                "type:drupal-library",
                "type:npm-asset",
                "type:bower-asset"
            ],
            "web/modules/contrib/{$name}": ["type:drupal-module"],
            "web/profiles/contrib/{$name}": ["type:drupal-profile"],
            "web/themes/contrib/{$name}": ["type:drupal-theme"],
            "drush/Commands/contrib/{$name}": ["type:drupal-drush"],
            "web/modules/custom/{$name}": ["type:drupal-custom-module"],
            "web/themes/custom/{$name}": ["type:drupal-custom-theme"]
        },
        "drupal-core-project-message": {
            "post-create-project-cmd-message": [
                "<bg=blue;fg=white>                                                                  </>",
                "<bg=blue;fg=white>  Congratulations, you’ve installed the Resource Hub codebase  </>",
                "<bg=blue;fg=white>                                                                  </>",
                "",
                "<bg=yellow;fg=black>Next steps</>:",
                "  1. Install the Drupal site (these steps require lando to be installed on your system): ",
                "    - Move into the project directory, e.g. cd resourcehub",
                "    - Bring up the Lando development environment: lando start",
                "    - Install Drupal: lando drush si resourcehub",
                "    - Browse to https://resourcehub.lndo.site",
                "  2. Remove the plugin that prints this message:",
                "      composer remove drupal/core-project-message"
            ]
        }

    }
}
