# A Composer project template for the Resource hub

This project is to be used with `composer create-project` to scaffold your Resource hub Drupal installation and bring in dependencies via Composer.

## Usage 

See the [Gitlab project](https://gitlab.com/openresources/resource-hub) for the Resource hub Drupal install profile for information on how to get started with Resource hub.
